
import React, { useReducer } from 'react';


// ************************************ CUSTOMIZED COMPONENTS ************************************ //

// TAREA
import TareaContext from './tareaContext';
import TareaReducer from './tareaReducer';


// ************************************ CUSTOMIZED COMPONENTS ************************************ //

import { 
    TAREAS_PROYECTO,
    AGREGAR_TAREA,
    VALIDAR_TAREA,
    ELIMINAR_TAREA,
    TAREA_ACTUAL,
    ACTUALIZAR_TAREA,
    LIMPIAR_TAREA
} from '../../types';

import clienteAxios from '../../config/axios';

// *********************************** THIRD PARTY PACKAGES ************************************** //


// *********************************************************************************************** //
const TareaState = props => {

    const initialState = {

        tareasproyecto: [],
        errortarea: false,
        tareaseleccionada: null

    }

    // CREATE DISPATCH AND STATE
    const [ state, dispatch ] = useReducer( TareaReducer, initialState );

    // CREATE FUNCTIONS

    // CREATE PROJECT'S TASK
    const obtenerTareas = async proyecto => {

        console.log( proyecto );

        try {

            const resultado = await clienteAxios.get('/api/tareas', { params: { proyecto } });

            console.log( resultado );

            dispatch({
                type: TAREAS_PROYECTO,
                payload: resultado.data.tareas
            })

        } catch (error) {
            
            console.error( error )

        }

    }

    // ADD A TASK TO THE SELECTED PROJECT
    const agregarTarea = async tarea => {

        console.log( tarea );

        try {

            const resultado = await clienteAxios.post( '/api/tareas', tarea );

            console.log( resultado );
            
            dispatch({
                type: AGREGAR_TAREA,
                payload: tarea
            })

        } catch (error) {
            console.error( error );
        }
    }

    // VALIDATE AND SHOWS AND ERROR IN CASE IT IS NECESSARY
    const validarTarea = () => {

        dispatch({
            type: VALIDAR_TAREA
        })

    }

    // DELETE TAKS BY ID
    const eliminarTarea = async ( id, proyecto ) => {

        try {

            await clienteAxios.delete( `/api/tareas/${id}`, { params: { proyecto } } )
            
            dispatch({
                type: ELIMINAR_TAREA,
                payload: id
            })

        } catch (error) {
            
            console.error(error);

        }

    }

    // EDIT'S OR MODIFIES A TASK
    const actualizarTarea = async tarea => {

        try {

            const resultado = await clienteAxios.put( `/api/tareas/${tarea._id}`, tarea )

            console.log( resultado );

            dispatch({
                type: ACTUALIZAR_TAREA,
                payload: resultado.data.tarea
            })
            
        } catch (error) {
            
            console.error(error);

        }

    }

    // EXTRACT TASK TO EDIT
    const guardarTareaActual = tarea => {

        dispatch({
            type: TAREA_ACTUAL,
            payload: tarea
        })

    }

    // DELETE tareaseleccionada
    const limpiarTarea = () => {

        dispatch({

            type: LIMPIAR_TAREA

        })

    }

    return (

        <TareaContext.Provider
            value={{
                tareasproyecto: state.tareasproyecto,
                errortarea: state.errortarea,
                tareaseleccionada: state.tareaseleccionada,
                obtenerTareas,
                agregarTarea,
                validarTarea,
                eliminarTarea,
                guardarTareaActual,
                actualizarTarea,
                limpiarTarea
            }}
        >
            { props.children }
        </TareaContext.Provider>

    )

}

export default TareaState;