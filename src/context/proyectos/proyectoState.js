import React, { useReducer } from 'react';

// ****************************** THIRD PARTY LIBRARIES ************************** //

import clienteAxios from '../../config/axios';


// ******************************************************************************* //

// ****************************** CUSTOMIZED COMPONENTS ************************** //

import proyectoContext from './proyectoContext';
import proyectoReducer from './proyectoReducer';

import {
    FORMULARIO_PROYECTO,
    OBTENER_PROYECTOS,
    AGREGAR_PROYECTO,
    PROYECTO_ERROR,
    VALIDAR_FORMULARIO,
    PROYECTO_ACTUAL,
    ELIMINAR_PROYECTO
} from '../../types';

// ******************************************************************************* //

const ProyectoState = props => {

    const initialState = {

        proyectos: [],
        formulario: false,
        errorformulario: false,
        proyecto: null,
        mensaje: null

    }

    // Dispatch to execute actions
    const [state, dispatch] = useReducer(proyectoReducer, initialState)

    // CRUD
    const mostrarFormulario = () => {

        dispatch({

            type: FORMULARIO_PROYECTO

        })

    }

    // GET PROJECTS
    const obtenerProyectos = async () => {

        try {

            const resultado = await clienteAxios.get( '/api/proyectos' )

            dispatch({
                type: OBTENER_PROYECTOS,
                payload: resultado.data.proyectos
            })
            
        } catch (error) {

            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }

            
            dispatch({
                type: PROYECTO_ERROR,
                payload: alerta
            })

        }

    }

    // ADD NEW PROJECT
    const agregarProyecto = async proyecto => {

        try {

            const resultado = await clienteAxios.post( '/api/proyectos', proyecto )

            console.log( resultado );

            // INSERTAR PROYECTO EN EL STATE
            dispatch({
                type: AGREGAR_PROYECTO,
                payload: resultado.data.proyecto
            })
            
        } catch (error) {

            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }

            
            dispatch({
                type: PROYECTO_ERROR,
                payload: alerta
            })

        }

        

    }

    // VERIFY FORM BY ERRORS
    const mostrarError = () => {

        dispatch({
            type: VALIDAR_FORMULARIO,

        })

    }

    // SELECT PROJECT WHERE USER CLICKED
    const proyectoActual = proyectoId => {

        dispatch({
            type: PROYECTO_ACTUAL,
            payload: proyectoId
        })
    }

    // DELETE A PROJECT
    const eliminarProyecto = async proyectoId => {

        try {

            await clienteAxios.delete( `/api/proyectos/${proyectoId}` )
            
            dispatch({
                type: ELIMINAR_PROYECTO,
                payload: proyectoId
            })

        } catch (error) {

            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }

            
            dispatch({
                type: PROYECTO_ERROR,
                payload: alerta
            })

        }
        

    }



    return (
        <proyectoContext.Provider
            value={{
                proyectos: state.proyectos,
                formulario: state.formulario,
                errorformulario: state.errorformulario,
                proyecto: state.proyecto,
                mensaje: state.mensaje,
                mostrarFormulario,
                obtenerProyectos,
                agregarProyecto,
                mostrarError,
                proyectoActual,
                eliminarProyecto
            }}
        >
            { props.children}
        </proyectoContext.Provider>
    )

}

export default ProyectoState;