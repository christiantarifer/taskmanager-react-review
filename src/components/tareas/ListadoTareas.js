import React, { Fragment, useContext } from 'react'

// ************************** CUSTOMIZED COMPONENTS ************************** //

import Tarea from './Tarea';

// *************************************************************************** //

// ************************************ CONTEXT ************************************ //

import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

// ********************************************************************************** //

// ******************************* THIRD PARTY PACKAGES *********************** //

import { CSSTransition, TransitionGroup } from 'react-transition-group';

// *************************************************************************** //

const ListadoTareas = () => {

    // GET FORM FROM THE STATE
    const proyectosContext = useContext(proyectoContext);

    const { proyecto, eliminarProyecto } = proyectosContext;

    // GET TAREA'S CONTEXT FUNCTIONS
    const tareasContext = useContext( tareaContext );
    const { tareasproyecto } = tareasContext;

    // IF THERE IS NOT PROJECT SELECTED
    if( !proyecto ) return <h2> Selecciona un proyecto </h2>

    // ARRAY DESTRUCTURING TO EXTRACT CURRENT PROJECT
    const [ proyectoActual ] = proyecto;
    
    // DELETE A PROJECT
    const onClickEliminar = () => {

        eliminarProyecto( proyectoActual._id )

    }

    return ( 

        <Fragment>

            <h2>Proyecto : { proyectoActual.nombre }</h2>

            <ul className="listado-tareas" >

                { tareasproyecto.length === 0
                    ? (  <li key="" className="tarea" > <p>No hay tareas</p> </li> )
                    : <TransitionGroup>
                        {tareasproyecto.map( tarea => ( 
                            <CSSTransition 
                                key={tarea._id}
                                timeout={200}
                                classNames="tarea"
                            >
                                <Tarea
                                    tarea={tarea}
                                />
                            </CSSTransition>
                        ))}
                    </TransitionGroup>

                 }

            </ul>

            <button 
                type="button"
                className="btn btn-eliminar"
                onClick={ onClickEliminar }
            > 
                Eliminar Proyecto &times;
            </button>
        </Fragment>

        

     );
}
 
export default ListadoTareas;