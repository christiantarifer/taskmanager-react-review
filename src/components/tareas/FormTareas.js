import React, { useContext, useEffect, useState } from 'react'

// ************************************ CONTEXT ************************************ //

import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

// ********************************************************************************** //

const FormTarea = () => {

    // GET PROJECT IF IT IS ACTIVE
    const proyectosContext = useContext(proyectoContext);
    const { proyecto } = proyectosContext;

    // GET TAREA'S CONTEXT FUNCTIONS
    const tareasContext = useContext( tareaContext );
    const { 
        tareaseleccionada, 
        errortarea,
        agregarTarea, 
        validarTarea,
        obtenerTareas,
        actualizarTarea,
        limpiarTarea
    } = tareasContext;

    // EFFECT DETECTS IF THERE A SELECTED TASK
    useEffect( () => {

        if( tareaseleccionada !== null ) {

            guardarTarea( tareaseleccionada )

        } else {

            guardarTarea({
                nombre: ''
            })

        }

    }, [ tareaseleccionada ] );
    

    // FORM STATE
    const [ tarea, guardarTarea ] = useState({

        nombre: ''    

    })

    // EXTRACT PROJECT NAME
    const { nombre } = tarea;
 
    // IF THERE IS NOT PROJECT SELECTED
    if( !proyecto ) return null;

    // ARRAY DESTRUCTURING TO EXTRACT CURRENT PROJECT
    const [ proyectoActual ] = proyecto;

    // READ FORM'S VALUES
    const handleChange = e => {

        guardarTarea({
            ...tarea,
            [ e.target.name ] : e.target.value
        })

    }

    const onSubmit = e => {

        e.preventDefault();


        // VALIDATE
        if ( nombre.trim() === '' ) {

            validarTarea();
            return;

        }

        // CHECK IF IT IS EDITION OR NEW TASK
        if( tareaseleccionada === null ) {

            // ADD NEW TASK TO TASK'S STATE
            tarea.proyecto = proyectoActual._id;
            agregarTarea( tarea );

        } else {

            // UPDATE CURRENT TASK
            actualizarTarea( tarea );

            // DELETE tareaseleccionada del state
            limpiarTarea();
            

        }

        // GET AND FILTER CURRENT PROJECT'S TASK
        obtenerTareas( proyectoActual.id );

        // RESET FORM
        guardarTarea({
            nombre : ''
        })

    }

    return ( 
        <div className="formulario" >

            <form
                onSubmit= { onSubmit }
                >

                <div className="contenedor-input">

                    <input 
                        type="text"
                        className="input-text"
                        placeholder="Nombre Area..."
                        name="nombre"
                        value={nombre}
                        onChange={handleChange}
                    />

                </div>

                <div className="contenedor-input" >

                    <input 
                        type="submit"
                        className="btn btn-primario btn-submit btn-block"
                        value={ tareaseleccionada ? 'Editar Tarea' : 'Agregar Tarea' }
                    />

                </div>

            </form>

            { errortarea ?  <p className="mensaje error"  > El nombre de la tarea es obligatorio.  </p> : null }

        </div>
     );
}
 
export default FormTarea;