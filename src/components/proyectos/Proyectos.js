import React, { useContext, useEffect } from 'react';

// ************************************ CUSTOMIZED COMPONENTS ************************************ //

import Sidebar from '../layout/Sidebar';
import Barra from '../layout/Barra';
import ListadoTareas from '../tareas/ListadoTareas';
import FormTarea from '../tareas/FormTareas';


// ********************************************* CONTEXT **************************************** //

import AuthContext from '../../context/autenticacion/authContext';

// *********************************************************************************************** //

const Proyectos = () => {

    // GET INFO FROM AUTHENTICATION
    const authContext = useContext( AuthContext );
    const { usuarioAutenticado } = authContext;

    useEffect( () => {

        usuarioAutenticado();

        // eslint-disable-next-line
    }, [  ] )

    return ( 

        <div className="contenedor-app">

            <Sidebar />

            <div className="seccion-principal">

                <Barra />

                <main>

                    <FormTarea />

                    <div className="contenedor-tareas">

                        <ListadoTareas />

                    </div>

                </main>

            </div>

        </div>

    );
}
 
export default Proyectos;