import React, { useContext, useEffect } from 'react';


// ************************************ CUSTOMIZED COMPONENT ************************************ //

import Proyecto from './Proyecto';
import proyectoContext from '../../context/proyectos/proyectoContext';
import AlertaContext from '../../context/alertas/alertaContext';

// *********************************************************************************************** //

// ******************************* THIRD PARTY PACKAGES *********************** //

import { CSSTransition, TransitionGroup } from 'react-transition-group';

// *************************************************************************** //

const ListadoProyectos = (  ) => {

     // Extract projects from initialState
     const proyectosContext = useContext(proyectoContext);
     const { mensaje, proyectos, obtenerProyectos } = proyectosContext;

     const alertaContext = useContext(AlertaContext);
     const { alerta, mostrarAlerta } = alertaContext;

    //  Get projects  when the component is loaded
     useEffect( () => {

        // IF THERE IS AN ERROR
        if( mensaje ){

            mostrarAlerta( mensaje.msg, mensaje.categoria );

        }

        obtenerProyectos();

        
        // eslint-disable-next-line
     }, [ mensaje ] );

    //  Check if project has contents
     if ( proyectos.length === 0 ) return <p> No hay proyectos, comienza creando uno. </p>;

     

    return ( 

        <ul className="listado-proyectos">

            { alerta ? (<div className={`alerta ${alerta.categoria}`}> {alerta.msg} </div>) : null }

            <TransitionGroup>

            { proyectos.map( proyecto =>  (

                <CSSTransition
                    key={proyecto._id}
                    timeout={200}
                    classNames="proyecto"
                >
                    <Proyecto        
                        proyecto={proyecto}
                    ></Proyecto>
                </CSSTransition>


            )) }

            </TransitionGroup>

        </ul>

     );
}
 
export default ListadoProyectos;