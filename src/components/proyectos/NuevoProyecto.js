import React, { Fragment, useContext, useState } from 'react';

// ************************************ CUSTOMIZED COMPONENTS ************************************ //

import proyectoContext from '../../context/proyectos/proyectoContext';

// ********************************************************************************************** //


const NuevoProyecto = () => {

    // GET FORM FROM THE STATE
    const proyectosContext = useContext(proyectoContext);

    const { formulario, errorformulario, mostrarFormulario, agregarProyecto, mostrarError } = proyectosContext;


    // PROJECT STATE
    const [ proyecto, guardarProyecto ] = useState({
        nombre: ''
    });

    // READ INPUT'S CONTENT
    const onChangeProyecto = e => {

        guardarProyecto({
            ...proyecto,
            [e.target.name] : e.target.value
        })

    }

    // EXTRACT PROJECT'S NAME
    const { nombre } = proyecto;

    // WHEN THE USER SEND A PROJECT
    const onSubmitProyecto = e => {

        e.preventDefault();

        // VALIDATE PROJECT
        if ( nombre === '' ) {

            mostrarError();

            return;

        }

        // ADD TO THE STATE
        agregarProyecto( proyecto )

        // RE START FORM
        guardarProyecto({

            nombre: ''

        })

    }

    // SHOW FORM
    const onClickFormulario = () => {
        mostrarFormulario()
    }


    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={ onClickFormulario }
            >
                Nuevo Proyecto
            </button>

            { 
                formulario
                ? (
                    <form
                        className="formulario-nuevo-proyecto"
                        onSubmit={onSubmitProyecto}
                    >
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Nombre Proyecto"
                            name="nombre"
                            value={nombre}
                            onChange={onChangeProyecto}
                        />

                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Agregar proyecto" 
                        />
                    </form>


                ) : null
            }

            { errorformulario ? <p className="mensaje error" > El nombre del proyecto es obligatorio. </p> : null }
        </Fragment>
    );
}
 
export default NuevoProyecto;