import React, { useContext } from 'react';

// ************************************ CUSTOMIZED COMPONENTS ************************************ //

import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';


// ********************************************************************************************** //

const Proyecto = ( {proyecto} ) => {

    // GET FORM FROM THE STATE
    const proyectosContext = useContext(proyectoContext);
    const { proyectoActual } = proyectosContext;

    // GET TAREA'S CONTEXT FUNCTIONS
    const tareasContext = useContext( tareaContext );
    const { obtenerTareas } = tareasContext;

    // FUNCTION TO ADD CURRENT PROJECT
    const seleccionarProyecto = id => {

        proyectoActual(id); // Set current project
        obtenerTareas(id);  // Filter task when clickedlas tareas cuando se de quit
        
    }


    return ( 

        <li  >

            <button
                type="button"
                className="btn btn-blank"
                onClick={ () => seleccionarProyecto( proyecto._id ) }
            >
                { proyecto.nombre }
            </button>

        </li>

     );
}
 
export default Proyecto;