import React, { useContext, useEffect } from 'react';

// ********************************************* CONTEXT **************************************** //

import AuthContext from '../../context/autenticacion/authContext';

// *********************************************************************************************** //

const Barra = () => {

    // GET INFO FROM AUTHENTICATION
    const authContext = useContext( AuthContext );
    const { usuario, usuarioAutenticado, cerrarSesion } = authContext;

    useEffect( () => {

        usuarioAutenticado();
        // eslint-disable-next-line
    }, [ ] );

    return ( 
        <header className="app-header">

            { usuario ? <p className="nombre-usuario" > Hola <span> { usuario.nombre } </span> </p> : null }
            

            <nav className="nav-principal" >

                <button
                    className="btn btn-primario cerrar-sesion"
                    onClick={ () => cerrarSesion() }
                    >
                        Cerrar Sesi&oacute;n
                </button>

            </nav>

        </header>
    );
}
 
export default Barra;