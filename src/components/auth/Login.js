import React, { useState, useContext, useEffect } from 'react';

// ************************************ THIRD PARTY LIBRARIES ************************************ //

import { Link } from 'react-router-dom';

// ************************************ THIRD PARTY LIBRARIES ************************************ //

import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/autenticacion/authContext';

// *********************************************************************************************** //

const Login = (props) => {

    // EXTRACT VALUES FROM CONTEXT
    const alertaContext = useContext( AlertaContext );
    const { alerta, mostrarAlerta } = alertaContext;

    const authContext = useContext( AuthContext );
    const { mensaje, autenticado, iniciarSesion } = authContext;

    // IN CASE USER IS ALREDY AUTHENTICATED O REGISTERED OR DUPLICATE REGISTER
    useEffect( () => {

        if ( autenticado ) {
            props.history.push( '/proyectos' );
        }

        if (mensaje) {
            mostrarAlerta( mensaje.msg , mensaje.categoria );
        }

        // eslint-disable-next-line
    }, [ mensaje, autenticado, props.history ] )

    // LOG IN STATE
    const [ usuario, guardarUsuario ] = useState({

        email: '',
        password: ''

    });

    // EXTRACT USER
    const { email, password } = usuario;

    const onChange = e => {

        guardarUsuario({

            ...usuario,
            [ e.target.name ]: e.target.value

        })

    };

    // USER START SESSION
    const onSubmit = e => {

        e.preventDefault();

        // VALIDATE EMPTY FIELDS
        if ( email.trim() === '' || password.trim() === '' ) {

            mostrarAlerta( 'Todos los campos son obligatorios', 'alerta-error' );

        }
        
        // PAST TO ACTION
        iniciarSesion( { email, password } );

    }

    return ( 
        
        <div className="form-usuario" >

            { alerta ? ( <div className={`alerta ${alerta.categoria}`}> {alerta.msg} </div> ) : null }

            <div className="contenedor-form sombra-dark" >

                <h1> Iniciar Sesi&oacute;n </h1>

                <form
                    onSubmit={onSubmit}
                >

                    <div className="campo-form">

                        <label htmlFor="email"> Email </label>

                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tu Email"
                            value={email}
                            onChange={onChange}
                         /> 

                    </div>

                    <div className="campo-form">

                        <label htmlFor="password"> Password </label>

                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tu password"
                            value={password}
                            onChange={onChange}
                        />

                    </div>

                    <div className="campo-form">

                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Iniciar Sesi&oacute;n"
                        />

                    </div>

                </form>

                <Link to={'/nueva-cuenta'} className="enlace-cuenta" >

                    Obtener Cuenta

                </Link>

            </div>

        </div>

    );
}
 
export default Login;