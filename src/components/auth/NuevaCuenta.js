import React, { useState, useContext, useEffect } from 'react';

// ************************************ THIRD PARTY LIBRARIES ************************************ //

import { Link } from 'react-router-dom';

// *********************************************************************************************** //

// ************************************ CONTEXT ************************************ //

import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/autenticacion/authContext';

// *********************************************************************************************** //

const NuevaCuenta = ( props ) => {

    // EXTRACT VALUES FROM CONTEXT
    const alertaContext = useContext( AlertaContext );
    const { alerta, mostrarAlerta } = alertaContext;

    const authContext = useContext( AuthContext );
    const { mensaje, autenticado, registrarUsuario } = authContext

    // IN CASE USER IS ALREDY AUTHENTICATED O REGISTERED OR DUPLICATE REGISTER
    useEffect( () => {

        if ( autenticado ) {
            props.history.push( '/proyectos' );
        }

        if (mensaje) {
            mostrarAlerta( mensaje.msg , mensaje.categoria );
        }

        
        // eslint-disable-next-line
    }, [ mensaje, autenticado, props.history ] )

    // LOG IN STATE
    const [ usuario, guardarUsuario ] = useState({

        nombre: '',
        email: '',
        password: '',
        confirmar: ''

    });

    // EXTRACT USER
    const { nombre, email, password, confirmar } = usuario;

    const onChange = e => {

        guardarUsuario({

            ...usuario,
            [ e.target.name ]: e.target.value

        })

    };

    // USER START SESSION
    const onSubmit = e => {

        e.preventDefault();

        // VALIDATE EMPTY FIELDS
        if ( nombre.trim() === '' ||
            email.trim() === '' ||
            password.trim() === '' ||
            confirmar.trim() === '') {
                mostrarAlerta( 'Todos los campos son obligatorios.', 'alerta-error' );
                return;
            }
        // MINIMUN 6 CHARACTERS PASSWORD
        if( password.length < 6 ) {
            mostrarAlerta( 'El password debe ser de al menos 6 caracteres.', 'alerta-error' );
            return;
        }

        // TWO PASSWORD MUST BE EQUAL
        if ( password !== confirmar ) {
            mostrarAlerta( 'Los password no son iguales.', 'alerta-error' );
            return;
        }
        
        // PAST TO ACTION
        registrarUsuario({ 
            nombre,
            email,
            password
         });

    }

    return ( 
        
        <div className="form-usuario" >
            { alerta ? ( <div className={`alerta ${alerta.categoria}`}> {alerta.msg} </div> ) : null }
            <div className="contenedor-form sombra-dark" >

                <h1> Obtener una cuenta </h1>

                <form
                    onSubmit={onSubmit}
                >

                    <div className="campo-form">

                        <label htmlFor="nombre"> Nombre </label>

                        <input
                            type="text"
                            id="nombre"
                            name="nombre"
                            placeholder="Tu Nombre"
                            value={nombre}
                            onChange={onChange}
                        /> 

                    </div>

                    <div className="campo-form">

                        <label htmlFor="email"> Email </label>

                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tu Email"
                            value={email}
                            onChange={onChange}
                         /> 

                    </div>

                    <div className="campo-form">

                        <label htmlFor="password"> Password </label>

                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tu password"
                            value={password}
                            onChange={onChange}
                        />

                    </div>

                    <div className="campo-form">

                        <label htmlFor="confirmar"> Confirmar Password </label>

                        <input
                            type="password"
                            id="confirmar"
                            name="confirmar"
                            placeholder="Repite tu password"
                            value={confirmar}
                            onChange={onChange}
                        />

                    </div>

                    <div className="campo-form">

                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Registrarme"
                        />

                    </div>
                    

                </form>

                <Link to={'/'} className="enlace-cuenta" >

                    Login

                </Link>

            </div>

        </div>

    );
}
 
export default NuevaCuenta;