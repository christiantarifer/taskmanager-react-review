import React from 'react';

// ************************************ THIRD PARTY LIBRARIES ************************************ //

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// ************************************ CUSTOMIZED COMPONENTS ************************************ //

// AUTH //
import Login from './components/auth/Login';
import NuevaCuenta from './components/auth/NuevaCuenta';


// PROYECTOS //
import Proyectos from './components/proyectos/Proyectos';

import RutaPrivada from './components/rutas/RutaPrivada';

// ********************************************* CONTEXT ***************************************** //

import ProyectoState from './context/proyectos/proyectoState'
import TareaState from './context/tareas/tareaState';
import AlertaState from './context/alertas/alertaState';
import AuthState from './context/autenticacion/authState';

// *********************************************  ***************************************** //

import tokenAuth from './config/tokenAuth';


// *********************************************************************************************** //

// CHECKS IF A TOKEN EXISTS
const token = localStorage.getItem( 'token' );

if ( token ) {

  tokenAuth( token );

}

function App() {

  console.log(process.env.REACT_APP_BACKEND_URL);

  return (
    <ProyectoState>
      <TareaState>
        <AlertaState>
          <AuthState>
            <Router>

              <Switch>

                <Route exact path="/" component={Login} />
                <Route exact path="/nueva-cuenta" component={NuevaCuenta}  />
                <RutaPrivada exact path="/proyectos" component={Proyectos}  />

              </Switch>

            </Router>
          </AuthState>
        </AlertaState>
      </TareaState>
    </ProyectoState>
  );
}

export default App;
